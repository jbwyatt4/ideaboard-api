# README

An Idea board app written in ReactJS that uses the Rails API.

Written by following Learnetto's tutorial for ReactJS and Rails API.

https://www.sitepoint.com/react-rails-5-1/

https://learnetto.com/tutorials/rails-5-api-and-react-js-tutorial-how-to-make-an-idea-board-app

## Rails

## ReactJS

All reactjs is in the client folder.

SASS preprocessing was added with:

https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-a-css-preprocessor-sass-less-etc
