bundle install --without production
cd client
npm start &
cd ..
mailcatcher
WID=$(xprop -root | grep "_NET_ACTIVE_WINDOW(WINDOW)"| awk '{print $5}')
xdotool windowfocus $WID
xdotool key ctrl+t #ctrl+shift+t
wmctrl -i -a $WID
rails s -p 3001
