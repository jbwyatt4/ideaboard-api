import React, { Component } from 'react'
import axios from 'axios'
import update from 'immutability-helper'

import Idea from './Idea'
import IdeaForm from './IdeaForm'

class IdeasContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ideas: [],
      editingIdeaId: null,
      notification: ''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:3001/api/v1/ideas.json')
    .then(response => {
      console.log(response)
      this.setState({ideas:response.data})
    })
    .catch(error => console.log(error))
  }

  addNewIdea = () => {
    axios.post(
      'http://localhost:3001/api/v1/ideas',
      {
        idea: {
          title: '',
          body: ''
        }
      }
    )
    .then(response => {
      console.log(response)
      // splice inserts the new data into response.data
      const ideas = update(this.state.ideas, {
        $splice: [[0, 0, response.data]]
      })
      // set the data as the new response
      this.setState({
        ideas: ideas,
        editingIdeaId: response.data.id
      });
    })
    .catch(error => console.log(error))
  }

  updateIdea = (idea) => {
    const ideaIndex = this.state.ideas.findIndex(x => x.id === idea.id)
    const ideas = update(this.state.ideas, {[ideaIndex]: { $set: idea }})
    this.setState({ideas: ideas, notification: 'All changes saved', transitionIn: true})
  }

  resetNotification = () => {
    this.setState({notification: ''})
  }

  enableEditing = (id) => {
    this.setState({editingIdeaId: id},
    () => { this.title.focus() })
    // set focus on title when you click on idea
  }

  deleteIdea = (id) => {
    axios.delete(`http://localhost:3001/api/v1/ideas/${id}`)
    .then(response => {
      const ideaIndex = this.state.ideas.findIndex(x => x.id ===id)
      const ideas = update(this.state.ideas, { $splice: [[ideaIndex,
      1]]})
      this.setState({ideas: ideas})
    })
    .catch(error => console.log(error))
  }

  render() {
    return (
      <div>
        <row>
          <button className="newIdeaButton"
            onClick={this.addNewIdea}>
            New Idea
          </button>
          <span className="notification">
            {this.state.notification}
          </span>
        </row>
        <br />
        <row>
          {this.state.ideas.map((idea) => {
            if(this.state.editingIdeaId === idea.id) {
              return(<IdeaForm idea={idea} key={idea.id}
                updateIdea={this.updateIdea}
                titleRef={input => this.title = input}
                resetNotification={this.resetNotification} />)
            } else {
              return(<Idea idea={idea} key={idea.id} onClick={this.enableEditing} onDelete={this.deleteIdea} />)
            }
          })}
        </row>
      </div>
    );
  }
}

export default IdeasContainer
